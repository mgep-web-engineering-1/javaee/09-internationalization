package edu.mondragon.webeng1.internationalization.resources;

import java.util.ListResourceBundle;

public class Resources_eu extends ListResourceBundle {
    private static final Object[][] contents = {
        { "string.hello", "Kaixo" },
        { "string.welcome", "Ongietorri" },
        { "string.currentLocale", "Momentuko hizkuntza" },
        { "string.browserAsking", "Nabigatzaileko hizkuntza" },
        { "language.en", "Ingelesa" },
        { "language.es", "Gaztelera" },
        { "language.eu", "Euskara" },
    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
