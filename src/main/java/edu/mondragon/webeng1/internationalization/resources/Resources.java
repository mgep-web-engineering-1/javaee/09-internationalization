package edu.mondragon.webeng1.internationalization.resources;

import java.util.ListResourceBundle;

public class Resources extends ListResourceBundle {
    private static final Object[][] contents = {
        { "string.hello", "Hello" },
        { "string.welcome", "Welcome" },
        { "string.currentLocale", "Current Language" },
        { "string.browserAsking", "Browser Locale" },
        { "language.en", "English" },
        { "language.es", "Spanish" },
        { "language.eu", "Basque" },
    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
