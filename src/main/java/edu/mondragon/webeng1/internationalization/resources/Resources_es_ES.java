package edu.mondragon.webeng1.internationalization.resources;

import java.util.ListResourceBundle;

public class Resources_es_ES extends ListResourceBundle {
    private static final Object[][] contents = {
        { "string.hello", "Hola" },
        { "string.welcome", "Bienvenido" },
        { "string.currentLocale", "Idioma Actual" },
        { "string.browserAsking", "Idioma del navegador" },
        { "language.en", "Inglés" },
        { "language.es", "Español" },
        { "language.eu", "Euskera" },
    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
