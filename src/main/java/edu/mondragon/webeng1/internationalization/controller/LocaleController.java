package edu.mondragon.webeng1.internationalization.controller;

import java.io.IOException;
import java.util.Locale;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.jsp.jstl.core.*;

@WebServlet(name = "LocaleController", urlPatterns = { "/locale" })
public class LocaleController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LocaleController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String language = request.getParameter("language");
		String country = request.getParameter("country");
		Locale newLocale;
		if (language != null && !language.equals("") && country != null && !country.equals("")) {
			newLocale = new Locale(language, country);
			Config.set(session, jakarta.servlet.jsp.jstl.core.Config.FMT_LOCALE, newLocale);
		}
		response.sendRedirect(request.getHeader("referer"));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
