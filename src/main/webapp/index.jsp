<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="browserLanguage" scope="page" value="${ pageContext.request.locale.language }" />
<c:set var="fmtLanguage" scope="page" value="${sessionScope['jakarta.servlet.jsp.jstl.fmt.locale.session']}" />

<!DOCTYPE html>
<c:choose>
  <c:when test="${not empty fmtLanguage}">
    <html lang="${fmtLanguage}">
  </c:when>
  <c:otherwise>
    <html lang="${browserLanguage}">
  </c:otherwise>
</c:choose>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>JSLT and Localization</title>
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <h1>JSLT and Localization</h1>
  <fmt:bundle basename="edu.mondragon.webeng1.internationalization.resources.Resources">
    <nav>
      <a href="/locale?language=en&country=UK">
        <fmt:message key="language.en" /></a>
      <a href="/locale?language=es&country=ES">
        <fmt:message key="language.es" /></a>
      <a href="/locale?language=eu&country=ES">
        <fmt:message key="language.eu" /></a>
    </nav>
    <section>
      <p>
        <fmt:message key="string.currentLocale" />:
        <c:out value="${fmtLanguage}"/>
      </p>
      <p>
        <fmt:message key="string.browserAsking" />:
        <c:out value="${browserLanguage}" />
        (
        <c:out value="${header['Accept-Language']}" />)
      </p>
      <p>
        <fmt:message key="string.hello" />
      </p>
      <p>
        <fmt:message key="string.welcome" />
      </p>
    </section>
  </fmt:bundle>
</body>

</html>