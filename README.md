# Retrieve Parameters in JSP

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

* The page shoud be available in 3 languages.
* By default, the page will load the language configured in the browser.
* When clicked in some links, a controller will change the language of the page.
* The controller will redirect back to the previous page.
* In order tp do that, all "strings" must be in 3 languages.

## Explaination

We will start the explaination from the navigation inside ```index.jsp```. We can find 3 links:

```jsp
<nav>
  <a href="/locale?language=en&country=UK">
  <fmt:message key="language.en" /></a>
  <a href="/locale?language=es&country=ES">
  <fmt:message key="language.es" /></a>
  <a href="/locale?language=eu&country=ES">
  <fmt:message key="language.eu" /></a>
</nav>
```

*(We will explain **fmt** tags later on).*

In this navigation, we can find 3 links, each of which will call to ```/locale``` (LocaleController) and will send a *language* and a *country* parameter.

### LocaleController.java

The controller will retrieve the 2 parameters from the request:

```java
...
String language = request.getParameter("language");
String country = request.getParameter("country");
...
```

If none of them is empty, it will create a Locale with them:

```java
...
Locale newLocale;
...
newLocale = new Locale(language, country);
...
```

JSTL has a library component called **FMT (Formatting Tag)** related to locale sensitive content. FMT lets us define the language of the application and that way, we can select the specific string for the selected language. This is how we define the language in the ```LanguageController```:

```java
...
Config.set(session, jakarta.servlet.jsp.jstl.core.Config.FMT_LOCALE, newLocale);
...
```

Finally, ```LanguageController``` will redirect us to the previous page.
This is interesting for larger applications, where we could change the language in any page.
Otherwise, we should select a specific page to redirect when language is changed (e.g. ```index.jsp```).
You can achieve that this way::

```java
...
response.sendRedirect(request.getHeader("referer"));
...
```

### JSP and FMT

In order to use FMT, we need to load the tag prefix:

```jsp
...
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
...
```

Now, we can set which string we want to print and depending on the language we have set to FMT, it will return a different string. In the example bellow we are printing 2 strings ```string.hello``` and ```string.welcome```. ```<fmt:bundle>``` tells the sistem where to go in order to find the definitions of those strings.

```jsp
<fmt:bundle basename="edu.mondragon.webeng1.internationalization.resources.Resources">
...
    <p>
        <fmt:message key="string.hello" />
    </p>
    <p>
        <fmt:message key="string.welcome" />
    </p>
</fmt:bundle>
```

So, we have to find ```string.hello``` and ```string.welcome``` inside ```...resources.Resources.java``` file.

### Resource files

That ```...resources.Resources.java``` has this constant:

```java
private static final Object[][] contents = {
    {"string.hello", "Hello"},
    {"string.welcome", "Welcome"},
    {"string.currentLocale", "Current Language"},
    {"string.browserAsking", "Browser Locale"},
    {"language.en", "English"},
    {"language.es", "Spanish"},
    {"language.eu", "Basque"},
  };
```

We can find ```string.hello``` and ```string.welcome``` strings there.

But, there are only english strings... What does FMT do to get multilingual Strings? We can find 2 other Java files next to ```Resources.java```:

* Resources_eu.java
* Resources_es_ES.java

You can see that the name is the same as the previous file but adding the language (and optionally the country) to the file name.
So, when we are asking for an Spanish content, it will search for the ```_es_ES``` or ```_es``` file, and if there is none, it will load the resources from ```Resources.java``` file (the default one).
In this case, we have decided that English is the default language of the application.

The country can be set if we have different strings for different countries. E.g.:

* Mexican (_es_MX) or Spanish (_es_ES)
* United Kingdom (_en_UK) or Unated States (_en_US)
* Southern Basque (_eu_ES) or Northern Basque (_eu_FR)

### Nav element

Going back to navigation, now we should be able to completely understand it:

```jsp
<fmt:bundle basename="edu.mondragon.webeng1.internationalization.resources.Resources">
...
<nav>
  <a href="/locale?language=en&country=UK">
  <fmt:message key="language.en" /></a>
  <a href="/locale?language=es&country=ES">
  <fmt:message key="language.es" /></a>
  <a href="/locale?language=eu&country=ES">
  <fmt:message key="language.eu" /></a>
</nav>
...
</fmt:bundle>
```

For example, we know that we have to search for ```language.en``` string in all resource files:

```java
// Resources.java
{"language.en", "English"},

//Resources_eu.java
{"language.en", "Ingelesa"},

//Resources_es_ES.java
{"language.en", "Inglés"},
```

Those will be the strings of the first ```nav > a``` depending on the language of the system.

### HTML lang property

The HTML tag should have the ```lang``` property set correctly for the 3 cases (en, eu, es).
Therefore, we can do the following:

Get the fmt locale and put it in the property:

```jsp
<c:set var="fmtLanguage" scope="page" value="${sessionScope['jakarta.servlet.jsp.jstl.fmt.locale.session']}" />
...
<html lang="${fmtLanguage}">
```

But there is a small problem.
What if we didn't set the locale yet?
What if we are happy with the locale of the browser?
Then, FMT locale won't be set, and it will show empty.
So we can get the locale of the request (the one the browser asked for) this way:

```jsp
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
...
<c:set var="browserLanguage" scope="page" value="${ pageContext.request.locale.language }" />
...
<html lang="${browserLanguage}">
```

**Browser can have a list of locales. Mine for example: ```eu,es;q=0.8,en-GB;q=0.6,en-US;q=0.4,en;q=0.2```. You can get it by ```header['Accept-Language']```.

But in reality, we want a combination. Browser's language by default, but update the language if the user calls LocaleController. That means:

```jsp
<c:set var="browserLanguage" scope="page" value="${ pageContext.request.locale.language }" />
<c:set var="fmtLanguage" scope="page" value="${sessionScope['jakarta.servlet.jsp.jstl.fmt.locale.session']}" />
...
<c:choose>
  <c:when test="${not empty fmtLanguage}">
    <html lang="${fmtLanguage}">
  </c:when>
  <c:otherwise>
    <html lang="${browserLanguage}">
  </c:otherwise>
</c:choose>
```

## Exercise

We will use the content in [07-jsp-templates](https://gitlab.com/mgep-web-engineering-1/07-jsp-templates). But instead of hiding-showing elements using JavaScript, we will create different pages and manage them using **JSP/Servlet/JSTL**.

### 01 - JSP & templates

Create 3 JSP files for the content (db.jsp, dbz.jsp and dbgt.jsp), one for the header and a last one for the footer.

### 02 - Controller

All the navigation elements call to the same controller, passing a parameter with the series you want to read about (e.g. /db?page=db). If nothing is passed as ```page``` parameter, controller should take ```db``` as default.

The controller should dispatch the page.

### 03 - Login, Session and Permission

Create an additional JSP, index.jsp that will have a login and a logout form.

Add also a controller for login and logout (a single controller for both actions).
If username and password are equal, store the username in the session and redirect to ```index.jsp``` with a success message. Otherwise, remove any username that was in the session and redirect to ```index.jsp``` with an error message.

```DbController``` should check if a username is in the session before dispatching the page. If there is no username in the session, redirect them to ```index.jsp``` with an error message.

Move all jsp files but ```index.jsp``` to ```WEB-INF``` folder so they are not accessible for any anonimous user.

### 04 - JSTL

ADD JSTL tags so there is no java code inside JSP files.

```index.jsp``` should show the login form if there is a username in the session, otherwise, itshould show the logout form. If there is any success or error messge in the session, show them and remove them from the session.

### 05 - FMT

Use JSTL/FMT for multilanguage. Make it available at least in 2 languages.

## Next and before

* Next [08-JSTL](https://gitlab.com/mgep-web-engineering-1/javaee/08-jstl)
* Before [10-dao-login](https://gitlab.com/mgep-web-engineering-1/javaee/10-dao-login)
